# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
def set_openai_async_session(where_err: str = __file__):
    import openai
    if not openai.aiosession.get():
        import asyncio
        import aiohttp
        import atexit

        sess = aiohttp.ClientSession()
        openai.aiosession.set(sess)

        def cleanup():
            try:
                asyncio.get_event_loop().run_until_complete(sess.close())
            except Exception as e:
                print(f"While calling 'set_openai_async_session' from {where_err}\n   Exception: {e}")

        atexit.register(cleanup)
    return openai
