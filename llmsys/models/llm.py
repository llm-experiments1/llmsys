# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from llmsys.models.interfaces import LLM
from llmsys.models.utils import set_openai_async_session
from llmsys.types_ import Prompt, Outcome, ChatTurn, format_chat_turn, format_response
from llmsys.utils import get_logger, count_tokens

log = get_logger(__name__)


class OpenAIChat(LLM):
    def __init__(self, model=None, temperature=0, separator: str | None = None):
        self.model = model or 'gpt-3.5-turbo'
        self.temperature = temperature
        openai = set_openai_async_session(__file__)
        self.sess = openai.ChatCompletion
        self.sep = separator

    async def __call__(self, prompt: Prompt | list[ChatTurn], model=None, temperature=0) -> Outcome:
        model = model or self.model
        temperature = temperature or self.temperature

        if isinstance(prompt, str):
            prompt = [ChatTurn(role='user', content=prompt)]

        log.debug('\n'.join(format_chat_turn(t) for t in prompt))
        completion = await self.sess.acreate(
            model=model,
            messages=prompt,
            temperature=temperature
        )
        response = completion.choices[0].message.content
        log.info(format_response(response))
        tcp = sum(count_tokens(p['content'], model) for p in prompt)
        tcr = count_tokens(response, model)
        log.debug(
            f"TOKEN COUNT (PROMPT | RESPONSE | TOT): {tcp} | {tcr} | {tcp + tcr}")
        return response
