# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
import asyncio
from typing import List

from llmsys.models.interfaces import Embedding
from llmsys.models.utils import set_openai_async_session
from llmsys.types_ import Prompt
from llmsys.utils import get_logger, count_tokens

log = get_logger(__name__)


class OpenAIEmbeddings(Embedding):

    @property
    def size(self) -> int:
        # see https://platform.openai.com/docs/guides/embeddings/second-generation-models
        if '-ada-' in self.model and '002' in self.model:
            return 1536
        if '-ada-' in self.model and '001' in self.model:
            return 1024
        if '-babbage-' in self.model:
            return 2048
        if '-curie-' in self.model:
            return 4096
        if '-davinci-' in self.model:
            return 12288
        raise Exception(f"Unknown embedding size for {self.model}.")

    def __init__(self, model=None):
        self.model = model or 'text-embedding-ada-002'
        openai = set_openai_async_session(__file__)
        self.embed = openai.Embedding

    async def __call__(self, prompts: Prompt | List[Prompt], model=None) -> List[List[float]]:
        model = model or self.model
        if not isinstance(prompts, (list, tuple)):
            prompts = [prompts]
        embeddings = []
        for call in await asyncio.gather(
                *[asyncio.create_task(self.embed.acreate(model=model, input=p)) for p in prompts]
        ):
            embeddings.append(call.data[0].embedding)
        tc = sum(count_tokens(p) for p in prompts)
        log.debug(f'TOKEN COUNT (EMBEDDINGS): {tc}')
        return embeddings
