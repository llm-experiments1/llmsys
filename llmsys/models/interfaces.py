# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 12/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from abc import ABC, abstractmethod
from typing import List, Iterable

from llmsys.types_ import Prompt, Outcome, ChatTurn


class Embedding(ABC):
    @property
    def size(self) -> int:
        return 0

    @abstractmethod
    async def __call__(self, prompts: List[Prompt], *args, **kwargs) -> List[Iterable[float]]:
        raise NotImplementedError


class LLM(ABC):
    @abstractmethod
    async def __call__(self, prompt: Prompt | list[ChatTurn], *args, **kwargs) -> Outcome:
        raise NotImplementedError
