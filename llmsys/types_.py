# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 12/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from typing import TypeVar, TypedDict, NewType, Literal

Prompt = TypeVar('Prompt', bound=str)
Outcome = TypeVar('Outcome', bound=str)


class _Ctx(list):
    def __str__(self):
        if self:
            s = 'Here is also an ordered list of turns that provide context:\n' + \
                '\n'.join(f"{i}. {c}" for i, c in enumerate(self, 1))
        else:
            s = 'There is no context available yet.'
        return s


Context = NewType('Context', _Ctx[str])
Context.__class__.__call__ = lambda self, x=None: _Ctx(x or [])


class ChatTurn(TypedDict):
    role: Literal["system", "user", "assistant", "function"]
    content: Prompt | Outcome


def format_chat_turn(turn: Prompt | ChatTurn) -> str:
    if isinstance(turn, dict):
        return f"@PROMPT[{turn['role']}]\n{turn['content']}"
    return f"@PROMPT\n{turn}"


def format_response(resp: Outcome) -> str:
    return f"@RESPONSE\n{resp}"
