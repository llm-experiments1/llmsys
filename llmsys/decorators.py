# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------


def deprecated(cls_or_func = None, *, raise_error_after: str = None):
    """
    Mark the object as subject to deprecation and eventual erasure.
    If the object is still in the codebase and is actively used past the
    erasure version, raise an exception to force acknowledging the problem.
    This is a decorator function.

    :param raise_error_after: the version after which this object will be erased.
    """
    import warnings
    import re
    from llmsys import constants

    raise_error_after = raise_error_after or constants.__version__
    vers = re.sub(r'[^\d\.]+', '', raise_error_after).strip()
    vers = tuple(int(x) if x else 0 for x in vers.split('.'))
    curr_vers = re.sub(r'[^\d\.]+', '', constants.__version__).strip()
    curr_vers = tuple(int(x) if x else 0 for x in curr_vers.split('.'))

    def decorator(cls_or_func):
        if curr_vers <= vers:
            warnings.warn(f'This object is deprecated and marked for deletion in version >= {raise_error_after}',
                          DeprecationWarning)
            return cls_or_func
        raise Exception(f'This object has been removed since version {raise_error_after}')

    if cls_or_func:
        return decorator(cls_or_func)
    return decorator
