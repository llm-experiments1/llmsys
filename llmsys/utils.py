# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 27/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
import logging
from inspect import getmodule, isfunction
from os import getenv


def count_tokens(text, model=None):
    if model and not isinstance(model, str) and hasattr(model, 'model'):
        model = model.model
    model = model or 'gpt-3.5-turbo'
    try:
        import tiktoken
        toks = tiktoken.encoding_for_model(model).encode(text)
        return len(toks)
    except:
        return -1


def qualname(obj):
    if isinstance(obj, type) or isfunction(obj):
        klass = obj
    else:
        klass = obj.__class__
    return f"{getmodule(obj).__name__}.{klass.__qualname__}"


def get_logger(name, level: str | int | None = None):
    if level is None:
        level = getenv('LOG_LEVEL', logging.DEBUG)

    if isinstance(level, str):
        level = {
            'DEBUG': logging.DEBUG,
            'INFO': logging.INFO,
            'WARNING': logging.WARNING,
            'ERROR': logging.ERROR,
            'CRITICAL': logging.CRITICAL,
        }.get(level, logging.INFO)

    log = logging.getLogger(name)
    if not any(isinstance(h, logging.StreamHandler) for h in log.handlers):
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter('%(filename)s:%(lineno)d %(levelname)s :: %(message)s'))
        log.addHandler(handler)
        handler.setLevel(level)

    log.setLevel(level)
    return log
