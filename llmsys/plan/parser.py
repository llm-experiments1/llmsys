# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 11/07/23
# @copyright Ludovico Frizziero
# -----------------------------------------
import asyncio
from enum import Enum, auto

from lark import Lark, Transformer, Discard

from llmsys.models.interfaces import LLM
from llmsys.plan.prompts import summarization_prompt
from llmsys.databases.vectors import VectorEngine, Hit
from llmsys.types_ import Outcome
from llmsys.utils import get_logger

log = get_logger(__name__)


class Op(Enum):
    TASK = auto()
    CALL1 = auto()
    CALL2 = auto()
    MEMORIZE = auto()
    RECALL = auto()
    ASK = auto()
    SAY = auto()
    END = auto()
    SUMMARY = auto()
    PUSH_RETVAL = auto()
    STRFMT = auto()


grammar = """
%import common.ESCAPED_STRING
_any: /[^ \(\);]+/ | ESCAPED_STRING 
_string: _any | _string "+"? _any

string_fmt: _string
    // | response
    // | recall 
    | _string "+"? response  
    | _string "+"? recall 
_arg: string_fmt
    | response
    | recall

response: "response" "(" ")"
call: "call" "(" _arg [";" _arg] ")"
memorize: "memorize" "(" _arg ";" _arg ")"
recall: "recall" "(" _arg (";" _arg)* ")"
ask: "ask" "(" _arg ")"
say: "say" "(" _arg ")"
end: "end" "(" _arg? ")"
summary: "summary" "(" _arg ";" _arg ")"
_body: task 
    | call 
    | memorize
    | recall
    | ask
    | say
    | summary
task: "task" "(" _arg ")" _body* end

%import common.WS
%ignore WS
start: task+
"""


class Program(Transformer):
    __slots__ = 'code', 'task_missing_end', 'stack', 'history'

    def __init__(self, llm: LLM, memory: VectorEngine, agent_cls: type | None = None, debug: bool = False):
        super().__init__()
        self.llm = llm
        self.memory = memory
        self.agent = agent_cls or type(self)
        self.debug = debug
        self.code = []
        self.stack = []
        self.history = {}
        self.task_missing_end = True
        self.lark = Lark(grammar, parser='lalr', transformer=self)

    def response(self, values):
        self.code.append(Op.PUSH_RETVAL)
        return 'response()'

    def string_fmt(self, values):
        string = ' '.join(values)
        self.code.append(string)
        if 'response()' in string:
            self.code.append(Op.STRFMT)
        return string

    def ask(self, values):
        self.code.append(Op.ASK)
        return Discard

    def say(self, values):
        self.code.append(Op.SAY)
        return Discard

    def call(self, values):
        if len(values) == 1:
            self.code.append(Op.CALL1)
        elif len(values) == 2:
            self.code.append(Op.CALL2)
        return Discard

    def memorize(self, values):
        self.code.append(Op.MEMORIZE)
        return Discard

    def recall(self, values):
        self.code.append(len(values))  # push arity of recall
        self.code.append(Op.RECALL)
        return 'response()'  # in case recall is used in a string_fmt operation

    def summary(self, values):
        self.code.append(Op.SUMMARY)
        return Discard

    def end(self, values):
        self.code.append(len(values))  # push arity of end
        self.code.append(Op.END)
        self.task_missing_end = False
        return Discard

    def task(self, values):
        if self.task_missing_end:
            self.code.append(0)
            self.code.append(Op.END)
        self.task_missing_end = True
        self.code.append(Op.TASK)
        return Discard

    def run(self, llm_response: Outcome = None) -> Outcome:
        return asyncio.get_event_loop().run_until_complete(self.arun(llm_response))

    async def arun(self, llm_response: Outcome = None) -> Outcome:
        if llm_response:
            self.lark.parse(llm_response)

        # this act as a CPU register for the return value of a function call
        last_response = None

        for op in self.code:
            if op is Op.TASK:
                arity = self.stack.pop()
                if arity == 1:
                    ret_val = self.stack.pop()
                else:
                    ret_val = None
                name = self.stack.pop()
                self.history[name] = ret_val
                last_response = ret_val
                log.debug(f'TASK({name}) -> {ret_val=}')
            elif op is Op.CALL2 or op is Op.CALL1:
                context = self.stack.pop() if op is Op.CALL2 else ''
                objective = self.stack.pop()
                result = await self.agent(self.llm, self.memory, self.__class__).do_call(objective, context)
                last_response = result
                log.debug(f'CALL({objective=}; {context=}) -> {result}')
            elif op is Op.MEMORIZE:
                hint = self.stack.pop()
                data = self.stack.pop()
                await self.memory.index(f"{data} [[{hint=}]]")
                log.debug(f"MEMORIZE({data=}, {hint=})")
            elif op is Op.RECALL:
                varargs = self.stack.pop()
                hints = [self.stack.pop() for _ in range(varargs)][::-1]
                data = await self.memory.query(hints)
                out = []
                for hits in data.values():
                    x = next(iter(hits), Hit()).text
                    out.append(x)
                last_response = ', '.join(out)
                log.debug(f"RECALL({hints=}) -> {last_response}")
            elif op is Op.ASK:
                question = self.stack.pop()
                if self.debug:
                    data = 'debug fake input'
                else:
                    data = input(question + ' ')
                last_response = data
                log.debug(f"ASK({question=}) -> {data}")
            elif op is Op.SAY:
                phrase = self.stack.pop()
                print(phrase)
                log.debug(f"SAY({phrase=})")
            elif op is Op.END:
                log.debug('END()')
            elif op is Op.SUMMARY:
                text = self.stack.pop()
                query = self.stack.pop()
                summary = await self.llm(summarization_prompt.format(query=query, text=text))
                last_response = summary
                log.debug(f'SUMMARY({query=}, {text=}) -> {summary}')
            elif op is Op.PUSH_RETVAL:
                self.stack.append(last_response)
                log.debug(f'PUSH_RETVAL({last_response=})')
            elif op is Op.STRFMT:
                string = self.stack.pop()
                resp = self.stack.pop()
                formatted = string.replace('response()', resp).replace('response', resp)
                self.stack.append(formatted)
                log.debug(f'STRFMT({string=}, {last_response=}) -> {formatted}')
            else:
                # we are treating an unknown op as a PUSH operation of a constant onto the stack
                self.stack.append(op)

        return '\n'.join(
            f"task {k!r} resulted in {v!r}" for k, v in self.history.items()
        )
