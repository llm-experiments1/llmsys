from abc import ABC, abstractmethod
from typing import Iterator


class Reader(ABC):
    def __init__(self, text):
        pass

    @abstractmethod
    def peek(self, idx: int = 0):
        raise NotImplementedError

    @abstractmethod
    def __next__(self):
        raise NotImplementedError


class SimpleReader(Reader):
    __slots__ = 'content', 'curr', 'max',

    def __init__(self, text: str | Iterator[str]):
        super().__init__(text)
        self.content = text if isinstance(text, str) else ''.join(text)
        self.curr = 0
        self.max = len(self.content)

    def peek(self, idx: int = 0):
        try:
            return self.content[self.curr + idx]
        except IndexError:
            return None

    def __iter__(self):
        return self

    def __next__(self):
        if self.curr < self.max:
            c = self.content[self.curr]
            self.curr += 1
            return c
        raise StopIteration

    def __repr__(self):
        return f"...{self.content[self.curr - 3:self.curr]} !{self.content[self.curr:self.curr+1]}! {self.content[self.curr + 1:self.curr + 10]}..."


class StreamingReader(Reader):
    """Far far slower but less memory intense reader"""
    __slots__ = "content", "buffer", "buff_len"

    def __init__(self, text: str | Iterator[str]):
        super().__init__(text)
        self.content = iter(text)
        self.buffer = None
        self.buff_len = 0

    def peek(self, idx: int = 0):
        if self.buffer and self.buff_len > idx:
            self.buffer = list(self.buffer)
        else:
            self.buffer = list(self.buffer or [])
            extend = [next(self.content, None) for _ in range(idx - len(self.buffer) + 1)]
            self.buffer.extend([x for x in extend if x is not None])
        try:
            return self.buffer[idx]
        except IndexError:
            return None

    def __iter__(self):
        return self

    def __next__(self):
        if self.buffer:
            if not isinstance(self.buffer, Iterator):
                self.buffer = iter(self.buffer)
            try:
                c = next(self.buffer)
            except StopIteration:
                self.buffer = None
                c = next(self.content)
        else:
            c = next(self.content)
        print(c, end='')
        return c
