# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 30/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
_old_game_prompt = """
Let's play a game. You have the followings moves:
- task(#objective#): task can be followed with a list of moves to fulfill #objective#, with one more visual indentation. A task must always terminate with an end move.
- call(#objective#; #context#): if a task is too complex for you or you lack proper information, create an objective and a context and call for help. Will return a response.
- memorize(#text to remember#; #hint#): allows to remember a text for long time. #hint# allows you to retrieve from memory more easily.
- recall(#hint#; ...): allows to recall a text similar to hint, stored for long time in memory. There can be multiple hints separated by ";". Will return a response.
- ask(#question#): allow to ask the other player a question or further clarification. Will return the other player's response.
- say(#text#): write some text output to the other player.
- end(#outcome#): signal the end of a task, returning its outcome. Will return a response.
- summary(#query#; #long text#): create a summary of the long text based on a specific query. Will return a response.
- response(): allows to get the response text from the previous move.
Constraints:
1. Prefer calling for help or recalling from memory when you do not know an answer, rather than to invent a response.
2. Shorter tasks (with less moves) are better than longer tasks.
3. You can create multiple tasks if the problem requires multiple steps to be solved.
4. A task must always terminate with an end move.
5. You can only complete a game. Do not do anything else.

An example game:
- task(find what is the other player favourite color)
  - ask(what is your favourite color?)
  - memorize(response(); favourite color)
  - end(response())
Another example:
- task(what is the Mona Lisa?)
  - call(find information about the Mona Lisa)
  - summary(what is the mona lisa?; response())
  - end(response())
Another example:
- task(order a pizza)
  - ask(what pizza would you like?)
  - memorize(response(); the pizza to order)
  - ask(at what hour would you like it delivered?)
  - memorize(response(); the time of delivery)
  - call(please order a pizza with the provided information; recall(the pizza to order; at what time))
  - end(response())
- task(recap the order)
  - recall(the pizza to order)
  - summary(when and what pizza has been ordered?; response())
  - say(Enjoy your response())
  - end()

You now have to complete the game that starts as:
- task({assigned_task})"""

refined_game_prompt = """
Your objective is to solve a problem by playing a game. You have the followings moves:
- task(#objective#): task can be followed with a list of moves to fulfill #objective#, with one more visual indentation. A task must always terminate with an end move.
- call(#objective#; #context#): if a task is too complex for you or you lack proper information, create an objective and a context and call for help. You must learn or use the response.
- memorize(#text to remember#; #hint#): allows to learn some acquired information. #hint# allows you to retrieve later from memory more easily.
- recall(#hint#; ...): allows to recall a text similar to hint, stored for long time in memory. There can be multiple hints separated by ";". You must use the response.
- ask(#question#): allow to ask the other player a question or further clarification. You must learn the response.
- say(#text#): write some text output to the other player.
- end(#outcome#): signal the end of a task, returning its outcome as a response.
- summary(#query#; #long text#): create a summary of the long text based on a specific query. You can directly use the response.
- response(): allows to get the response text from the previous move.
Constraints:
1. Prefer calling for help or recalling from memory when you do not know an answer, rather than to invent a response.
2. Ask yourself if the problem requires multiple steps to be solved. If so you can create multiple tasks.
3. Shorter tasks (with less moves) are better than longer tasks.
4. A task must always terminate with an end move.
5. You can only complete a game. Do not do anything else.

An example game:
PROBLEM:
what's the player's favourite color
---
task(find what is the other player favourite color)
  ask(what is your favourite color?)
  memorize(response(); favourite color)
  end(response())
  
  
Another example:
PROBLEM:
order a pizza
---
task(order a pizza)
  ask(what pizza would you like?)
  memorize(response(); the pizza to order)
  ask(at what hour would you like it delivered?)
  memorize(response(); the time of delivery)
  call(please order a pizza with the provided information; recall(the pizza to order; at what time))
  end(response())
task(recap the order)
  recall(the pizza to order)
  summary(when and what pizza has been ordered?; response())
  say(Enjoy your response())
  end()

Now complete this game:
PROBLEM:
{assigned_task}
---
task("""

game_nocall_prompt = """
Let's play a game. You have the followings moves:
- task(#objective#): task can be followed with a list of moves to fulfill #objective#, with one more visual indentation. A task must always terminate with an end move.
- memorize(#text to remember#; #hint#): allows to remember a text for long time. #hint# allows you to retrieve from memory more easily.
- recall(#hint#; ...): allows to recall a text similar to hint, stored for long time in memory. There can be multiple hints separated by ";". Will return a response.
- ask(#question#): allow to ask the other player a question or further clarification. Will return the other player's response.
- say(#text#): write some text output to the other player.
- end(#outcome#): signal the end of a task, returning its outcome. Will return a response.
- summary(#query#; #long text#): create a summary of the long text based on a specific query. Will return a response.
- response(): allows to get the response text from the previous move.
Constraints:
1. Prefer recalling from memory when you do not know an answer, rather than to invent a response.
2. Ask yourself if the problem requires multiple steps to be solved. If so you can create multiple tasks.
3. Shorter tasks (with less moves) are better than longer tasks.
4. A task must always terminate with an end move.
5. You can only complete a game. Do not do anything else.

An example game:
task(search all documents a user needs)
    ask(keywords to search for in the database?)
    recall(response())
    summary(for each document, state the title and a brief abstract; response())
    end(response())
    
Another example game:
task(help the user)
    ask(what do you need my help for?)
    memorize(response(); reason for help)
    end()
task(produce an answer)
    summary(answer to the user needs; recall(reason for help))
    end(response())

PROBLEM:
{assigned_task}

To solve the problem, continue the game that starts as:
task("""

game_prompt = refined_game_prompt

# a slight variation of the game_prompt. Seems to yield worse results though.
plan_prompt = """
You have to come up with a plan to solve a problem. You have the followings moves:
- task(#objective#): task can be followed with a list of moves to fulfill #objective#, with one more visual indentation.
- call(#objective#; #context#): if a task is too complex for you or you lack proper information, create an objective and a context and call for help. Will return a response.
- memorize(#text to remember#; #hint#): allows to remember a text for long time. #hint# allows you to retrieve from memory more easily.
- recall(#hint#; ...): allows to recall a text similar to hint, stored for long time in memory. There can be multiple hints separated by ";". Will return a response.
- ask(#question#): allow to ask the other player a question or further clarification. Will return the other player's response.
- say(#text#): write some text output to the other player.
- end(#outcome#): signal the end of a task, returning its outcome. Will return a response.
- summary(#query#; #long text#): create a summary of the long text based on a specific query. Will return a response.
- response(): allows to get the response text from the previous move.
Constraints:
1. Prefer calling for help or using memory when you do not know an answer, rather than to invent a response.
2. Shorter tasks (with less moves) are better than longer tasks.
3. You can create multiple tasks if the problem requires multiple steps to be solved.
4. A task must always terminate with an end move.
5. You can only complete a plan. Do not do anything else.

An example plan:
- task(find what is the other player favourite color)
  - ask(what is your favourite color?)
  - memorize(response(); favourite color)
  - end(response())
Another example:
- task(what is the Mona Lisa?)
  - call(find information about the Mona Lisa)
  - summary(what is the mona lisa?; response())
  - end(response())
Another example:
- task(order a pizza)
  - ask(what pizza would you like?)
  - memorize(response(); the pizza to order)
  - ask(at what hour would you like it delivered?)
  - memorize(response(); the time of delivery)
  - recall(the pizza to order; at what time)
  - call(please order a pizza with the provided information; response())
  - end(response())
- task(recap the order)
  - recall(the pizza to order)
  - summary(what pizza has been ordered?; response())
  - say(Enjoy your response())
  - end()

You now have to complete the plan that starts as:
- task({assigned_task})"""

summarization_prompt = """
CONTEXT: 
{query}

TEXT:
{text}

Taking the context into consideration, create a short summary of the provided text. 
Only write the summary and nothing else.
"""

tools_prompt = """
You have the following tools available:
- decline(#motif#; #hint#): decline a request because you lack the capacity to fulfill it. Provide a motif and then an hint on how to fix the problem.
- python(#description#): describe something that has to be coded using python. Do not code yourself.  
- html(#description#): describe something that has to be coded using html. Do not code yourself.  
- google(#query#): issue a google search query.
- generate(#query#): automatically generate a response to a user query. Preferable for simple queries that are not asking about facts.

Examples:
- decline(I cannot find what's the current weather without knowing the location; find the location)
- python(create a small script to train a random forest. Use made up data as training set.)
- html(create a simple welcome page that says 'hello' and has a footer with copyright information.)
- google(what is the mona lisa?)
- generate(tell me a joke)

CONSTRAINTS:
1. Be as specific as possible when choosing a tool. 
2. Prefer declining if the task is too complicated or requires to many steps.

CONTEXT:
{context}

TASK:
{task}

Taking into consideration the context and task, choose which tool to use. 
Only respond in a similar way to the provided examples.
"""

python_prompt = """
Write a python script that fulfills the provided requirements.
The first line of the script must be a comment stating the file name to give to the script as '# filename: <filename>'.
Make sure the script is enclosed in a triple-backtick python markdown field.
REQUIREMENTS:
{reqs}
"""
