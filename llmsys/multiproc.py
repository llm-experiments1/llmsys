# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from typing import Callable, Awaitable, Any

__all__ = (
    'dispatch',
)
_pexe = _texe = None


def _get_executors():
    """
    We need to make sure we don't spawn other pools while in child threads / processes
    """
    from multiprocessing import parent_process
    from threading import main_thread, current_thread

    global _pexe, _texe
    is_main = parent_process() is None and current_thread() == main_thread()
    if is_main and not (_pexe or _texe):
        # main process / thread
        from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
        import os
        import atexit

        _pexe = ProcessPoolExecutor()
        _texe = ThreadPoolExecutor(os.cpu_count())

        atexit.register(_pexe.shutdown)
        atexit.register(_texe.shutdown)
    return _pexe, _texe


def dispatch(func: Callable, *args, use_threads=True) -> Awaitable[Any]:
    """Dispatch a function call to the async event loop"""
    import asyncio
    pexe, texe = _get_executors()
    if use_threads:
        return asyncio.get_event_loop().run_in_executor(texe, func, *args)
    return asyncio.get_event_loop().run_in_executor(pexe, func, *args)
