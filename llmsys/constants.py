# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 13/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
try:
    with open('../VERSION') as f:
        __version__ = f.read()
except FileNotFoundError:
    __version__ = 'unknown'
