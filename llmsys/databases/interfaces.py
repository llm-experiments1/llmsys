# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 12/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from abc import ABC


class DBConn(ABC):
    """Force good-practises for database connections"""

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close()
        return exc_type is None

    async def close(self, *args, **kwargs):
        pass
