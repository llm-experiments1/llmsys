# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 12/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------
from abc import ABC, abstractmethod
from typing import List, Mapping, NamedTuple
from uuid import uuid4

from llmsys.databases.interfaces import DBConn
from llmsys.models.interfaces import Embedding
from llmsys.multiproc import dispatch
from llmsys.types_ import Prompt


class Hit(NamedTuple):
    text: str = ''
    score: float = 0


class VectorEngine(ABC):
    @abstractmethod
    async def index(self, *args, **kwargs) -> None:
        raise NotImplementedError

    @abstractmethod
    async def query(self, *args, **kwargs) -> Mapping[str, List[Hit]]:
        raise NotImplementedError


class MilvusEngine(VectorEngine, DBConn):

    def __init__(
            self,
            embeddings: Embedding,
            collection_name: str | None = None,
            host: str | None = None,
            port: str | None = None
    ):
        from pymilvus import connections, Collection, CollectionSchema, FieldSchema, DataType

        self.embed = embeddings
        self.alias = str(uuid4())
        self.collection_name = collection_name or 'default'
        connections.connect(alias=self.alias, host=host or 'localhost', port=port or '19530')
        self.collection = Collection(
            name=self.collection_name,
            using=self.alias,
            schema=CollectionSchema(
                fields=[
                    FieldSchema(name='id', dtype=DataType.INT64, is_primary=True),
                    FieldSchema(name='vector', dtype=DataType.FLOAT_VECTOR, dim=embeddings.size),
                    FieldSchema(name='document', dtype=DataType.VARCHAR, max_length=2 ** 16 - 1),
                ],
                description='Simple Text2Vector index',
                enable_dynamic_field=False
            )
        )
        if not self.collection.has_index(index_name='default'):
            self.collection.create_index(
                field_name='vector',
                index_params={"metric_type": "IP", "index_type": "IVF_FLAT", "params": {"nlist": 1024}},
                index_name='default'
            )

    async def index(self, documents: Prompt | List[Prompt]):
        if not isinstance(documents, (list, tuple)):
            documents = [documents]
        vectors = await self.embed(documents)
        fut = self.collection.insert(
            [
                [hash(x) for x in documents],
                vectors,
                documents,
            ],
            _async=True
        )
        await dispatch(fut.result)
        self.collection.flush()

    async def query(self, queries: Prompt | List[Prompt], limit: int = 1) -> Mapping[str, List[Hit]]:
        if not isinstance(queries, (list, tuple)):
            queries = [queries]

        search_params = {"metric_type": "IP", "params": {"nprobe": 10}}
        data = await self.embed(queries)

        self.collection.load()
        fut = self.collection.search(
            data=data,
            anns_field='vector',
            param=search_params,
            limit=limit,
            output_fields=['document'],
            consistency_level='Strong',
            _async=True
        )
        results = await dispatch(fut.result)
        out = [
            [Hit(hit.entity.get('document'), hit.score) for hit in hits]
            for hits in results
        ]
        self.collection.release()
        return dict(zip(queries, out))

    async def close(self, *args, **kwargs):
        from pymilvus import connections
        self.collection.flush()
        connections.disconnect(self.alias)
