A fun example meant to experiment with a concept akin to AutoGPT.

The aim is to have an agent able to plan its actions, comprising long term memorization and function calling, to solve
some task autonomously.

The plan is represented by a language that is parsable and executable by a stack-based VM like any other programming
language, but it should be designed ideally to be as talkative as possible, to leverage the insane ability LLMs have to
generate structured text. It cannot be python or any other existing general purpose programming languages, because they
are too powerful and
expressive.

> Not planning (feel like there is a pun here 🤣) to extract anything really meaningful out of this, for the time being
> at least. Just experimenting!