# -----------------------------------------
# @author Ludovico Frizziero <ludovico.frizziero@gmail.com>
# @date 23/06/23
# @copyright Ludovico Frizziero
# -----------------------------------------

from dotenv import find_dotenv, load_dotenv

from llmsys.plan.parser import Program

load_dotenv(find_dotenv())
import asyncio

from llmsys.models.embeddings import OpenAIEmbeddings
from llmsys.models.llm import OpenAIChat
from llmsys.databases.vectors import MilvusEngine
from llmsys.utils import get_logger

log = get_logger(__name__)


async def main():
    try:
        while True:
            async with MilvusEngine(embeddings=OpenAIEmbeddings()) as db:
                llm = OpenAIChat()
                agent = Program(llm, db)
                mission = input('SET THE MISSION (type quit() to exit): ').strip()
                if mission == 'quit()':
                    break
                mission = mission or 'chat with the user'
                outcome = await agent.do_task(mission)
                log.info(f'@FINAL ANSWER\n{outcome}')
    finally:
        from pymilvus import utility, connections
        connections.connect()
        utility.drop_collection(db.collection_name)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
